public with sharing class CustomNotificationFromApex {

    public static void notifyUsers() {
        // Create a new custom notification
        Messaging.CustomNotification notification = new Messaging.CustomNotification();

        // Set the contents for the notification
        notification.setTitle('Custom Title');
        notification.setBody('Custom body');

        // Set the notification type and target
        notification.setNotificationTypeId(customNotificationType);//you have to create a custom notification type from setup
        notification.setTargetId(targetId);// Any record id which you want to link it to
        
        // Actually send the notification
        try {
            notification.send(getUserIds());
        }
        catch (Exception e) {
            System.debug('Problem sending notification: ' + e.getMessage());
        }
    }
}